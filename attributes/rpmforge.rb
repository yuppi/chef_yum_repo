#
# レシピで使用する変数を設定する
#
include_attribute 'yum_repo::default'

# ソースコードの URL
default['yum_repo']['rpmforge']['repository_url']      = 'http://pkgs.repoforge.org/rpmforge-release/'
default['yum_repo']['rpmforge']['source_file_name']    = "rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm"