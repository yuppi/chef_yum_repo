#
# レシピで使用する変数を設定する
#
include_attribute 'yum_repo::default'

# ソースコードの URL
default['yum_repo']['epel']['repository_url']      = 'https://dl.fedoraproject.org/pub/epel/6/x86_64/'
default['yum_repo']['epel']['source_file_name']    = "epel-release-6-8.noarch.rpm"