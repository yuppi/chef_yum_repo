#
# レシピで使用する変数を設定する
#
include_attribute 'yum_repo::default'

# ソースコードの URL
default['yum_repo']['remi']['repository_url']      = 'http://rpms.famillecollet.com/enterprise/'
default['yum_repo']['remi']['source_file_name']    = "remi-release-6.rpm"