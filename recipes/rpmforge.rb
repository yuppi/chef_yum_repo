#
# Cookbook Name:: yum_repo
# Recipe:: rpmforge
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# RPMForgeリポジトリファイルをDL＠RHEL6
#--------------------------------------------------------------------
remote_file node['yum_repo']['default']['work_dir'] + node['yum_repo']['rpmforge']['source_file_name'] do
    source node['yum_repo']['rpmforge']['repository_url'] + node['yum_repo']['rpmforge']['source_file_name']
    not_if "ls #{node['yum_repo']['default']['work_dir']}#{node['yum_repo']['rpmforge']['source_file_name']}"
end

#--------------------------------------------------------------------
# RPMForgeリポジトリを追加する
#--------------------------------------------------------------------
# RPMをインストール
rpm_package "yum-repo-rpmforge-rpm" do
    action :install
    source node['yum_repo']['default']['work_dir'] + node['yum_repo']['rpmforge']['source_file_name']
end