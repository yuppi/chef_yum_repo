#
# Cookbook Name:: yum_repo
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# yum用リポジトリを置くディレクトリを作成
#--------------------------------------------------------------------
directory node['yum_repo']['default']['work_dir'] do
    action :create
    recursive true
    not_if "ls -d #{node['yum_repo']['default']['work_dir']}"
end