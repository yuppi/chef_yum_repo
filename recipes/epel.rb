#
# Cookbook Name:: yum_repo
# Recipe:: epel
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# EPELリポジトリファイルをDL＠RHEL6
#--------------------------------------------------------------------
remote_file node['yum_repo']['default']['work_dir'] + node['yum_repo']['epel']['source_file_name'] do
    source node['yum_repo']['epel']['repository_url'] + node['yum_repo']['epel']['source_file_name']
    not_if "ls #{node['yum_repo']['default']['work_dir']}#{node['yum_repo']['epel']['source_file_name']}"
end

#--------------------------------------------------------------------
# EPELリポジトリを追加する
#--------------------------------------------------------------------
# RPMをインストール
rpm_package "yum-repo-epel-rpm" do
    action :install
    source node['yum_repo']['default']['work_dir'] + node['yum_repo']['epel']['source_file_name']
end