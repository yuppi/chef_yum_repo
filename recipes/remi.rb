#
# Cookbook Name:: yum_repo
# Recipe:: remi
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# remiリポジトリファイルをDL＠RHEL6
#--------------------------------------------------------------------
remote_file node['yum_repo']['default']['work_dir'] + node['yum_repo']['remi']['source_file_name'] do
    source node['yum_repo']['remi']['repository_url'] + node['yum_repo']['remi']['source_file_name']
    not_if "ls #{node['yum_repo']['default']['work_dir']}#{node['yum_repo']['remi']['source_file_name']}"
end

#--------------------------------------------------------------------
# remiリポジトリを追加する
#--------------------------------------------------------------------
# RPMをインストール
rpm_package "yum-repo-remi-rpm" do
    action :install
    source node['yum_repo']['default']['work_dir'] + node['yum_repo']['remi']['source_file_name']
end